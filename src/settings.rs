use std::env;

use config::{Config, ConfigError, Environment, File};
use serde_derive::Deserialize;

#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct GlobalConfig {
    #[serde(rename = "cycle-frequency")]
    pub cycle_frequency: u64,
    #[serde(rename = "app-name")]
    pub app_name: String,
    #[serde(rename = "app-labels")]
    pub app_labels: Option<Vec<MetricLabel>>,
}

#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct Gauge {
    pub name: String,
    pub description: String,
    pub labels: Option<Vec<MetricLabel>>,

    #[serde(rename = "update-frequency-average")]
    pub update_frequency_average: f64,

    #[serde(rename = "low-pct")]
    pub low_pct: f64,

    #[serde(rename = "high-pct")]
    pub high_pct: f64,
    pub min: i16,
    pub max: i16,
}

#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct Counter {
    pub name: String,
    pub description: String,
    pub labels: Option<Vec<MetricLabel>>,
    #[serde(rename = "update-frequency-average")]
    pub update_frequency_average: f64,

    #[serde(rename = "reset-at-cycles")]
    pub reset_counter: Option<u64>,
    pub starting: u64,
    pub increment: CounterIncrement,
}

#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct CounterIncrement {
    pub amount: f64,
    pub low: f64,
    pub high: f64,
}

#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct MetricLabel {
    pub key: String,
    pub value: String,
}


#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct Histogram {
    pub name: String,
    pub description: String,
    pub labels: Option<Vec<MetricLabel>>,
    #[serde(rename = "update-frequency-average")]
    pub update_frequency_average: f64,
    #[serde(rename = "min-duration")]
    pub min_duration: u64,
    #[serde(rename = "max-duration")]
    pub max_duration: u64,
    #[serde(rename = "count-min")]
    pub count_min: u64,
    #[serde(rename = "count-max")]
    pub count_max: u64,
}

#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct MetricsConfig {
    pub gauges: Option<Vec<Gauge>>,
    pub counters: Option<Vec<Counter>>,
    pub histograms: Option<Vec<Histogram>>,
}

#[derive(Debug, Deserialize,Clone)]
#[allow(unused)]
pub struct AppConfig {
    pub metric_config: Option<String>,
    pub global: GlobalConfig,
    pub metrics: Option<MetricsConfig>,
}

impl AppConfig {
    pub fn new() -> Result<Self, ConfigError> {
        // chooses which config/*-metrics.yaml to use
        let metrics_config_file = env::var("METRIC_CONFIG").unwrap_or_else(|_| "generic".into());
        let config_folder = env::var("METRIC_CONFIG_FOLDER").unwrap_or_else(|_| "config".into());

        let s = Config::builder()
            .add_source(File::with_name(&format!("{config_folder}/app-config")))
            .add_source(
                File::with_name(&format!("{config_folder}/{metrics_config_file}-metrics"))
                    .required(false),
            )
            .add_source(Environment::with_prefix("APP"))
            .set_override("metric_config", format!("{config_folder}/{metrics_config_file}-metrics"))?
            .build()?;
        s.try_deserialize()
    }
}
