# Overview

This application produces metrics in Open Telemetry format through both an endpoint (prometheus model) and via logs (TBD).  
Configuration files provide the different gauge, counter, histograms and summary object types along with some configuration to control the values.

## Metrics

The project uses an ENV variable 'METRIC_CONFIG` to determine what configuration file to use when running. The value of 
`METRIC_CONFIG` is the prefix of the file with `-metrics.yaml` being the suffix.

To change the file, or when using Kubernetes to run, set the ENV `METRIC_CONFIG=generic` for the generic, or use `burgers` as the optional built-in. Create your own too!

### Cycles

The metrics are emitted based on a cycle loop. One cycle per `cycle-frequency` as defined in `${METRIC_CONFIG_FOLDER}/app-config.yaml`. 

METRIC_CONFIG_FOLDER is the ENV variable pointing to the folder which `app-config.yaml`, `*-metrics.yaml` files are located.

## Building

```bash
# Rust 1.72+
cargo build
```

### Sample .envrc file
```shell
export PROJECT_ID="<GCP PROJECT_ID>"
export REPO_REGION="<GCP REGION>" # Artifact Registry is located in
export REPO_FOLDER="demos"  # whatever bucket you want for Artifact Registry
export REPO_HOST="${REPO_REGION}-docker.pkg.dev"

export CONTAINER_NAME="demo-metric-emitter"

# Gitlab
export GITLAB_PROJECT="<GITLAB-PROJECT-NAME>"

# Google Artifact Registry
export IMAGE_URL="${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${CONTAINER_NAME}"

# Gitlab
# export IMAGE_URL="registry.gitlab.com/${GITLAB_PROJECT}/${CONTAINER_NAME}"

export IMAGE_LATEST_FULL="${IMAGE_URL}:latest"

export RUST_LOG="demo_metric_emitter=info"
export RUST_LOG_STYLE=always # never or auto
```

## Docker Image (local)

```bash
export CONTAINER_NAME="demo-metric-emitter"
docker build -t ${CONTAINER_NAME} .
```
```bash
# Run
docker run -rm ${CONTAINER_NAME}
# ctrl+c to end
```

# Publishing

Pushing the repo to an Artifact Registry (GCP) can be achieve with the code below. The AR needs to be created once, the publishing
can happen anytime you have a new change to the container.

> RECOMMEND: Installing dotfiles or direnv (preferred) to automatically set the ENV variables for the folder
> NOTE: GCP Artifact Registry needs a repo to deploy to. Create an artifact repo named `demos`

```bash
# Few simple environment variables (suggest adding these to .envrc or .env)
export PROJECT_ID="<PROJECT_ID>"
export REPO_REGION="us-west1"
export REPO_FOLDER="demos"  # whatever bucket you want for Artifact Registry
export REPO_HOST="${REPO_REGION}-docker.pkg.dev/${PROJECT_ID}/${REPO_FOLDER}/${NAME}:latest" # or could be gcr.io, but the below script will not work for gcr.io
export CONTAINER_NAME="demo-metric-emitter"

# Enable the service
gcloud services enable artifactregistry.googleapis.com
# Create a repo
gcloud artifacts repositories create ${REPO_FOLDER} --repository-format=docker --location=${REPO_REGION} --description="Docker repository"
# Make the repo public (OPTIONAL, ONLY do this if you feel comfortable with production-access to repo)
gcloud artifacts repositories add-iam-policy-binding ${REPO_FOLDER} --location="${REPO_REGION}" --member=allUsers --role="roles/artifactregistry.reader"
```

## Docker Image GCP Artifact Registry

```bash
./build.sh 1 # 1 = version you want to publish
```

## Deploy to K8s

While not sophisticated, this `kustomize` command will deploy to the current kubeconfig context

```bash
# (option 1, if you want to see manifests) Generate manifests (assuming all environment variables are set using .envrc or .env)
kustomize build | envsubst > manifests/demo-metric-emitter.yaml

# (option 1)
kubectl apply -f manifests/

# (Option 2) Apply w/ kustomize
kustomize

# (Option 3) Apply using ConfigSync (add to RepoSync or RootSync)
```

## Setting up Monitoring w/ Google Managed Prometheus

1. Create a GSA to write to GMP
```bash
export GSA_NAME="gmp-writer"
gcloud iam service-accounts create ${GSA_NAME}
```

1. Give GSA permissions to write
```bash
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member=serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com --role=roles/monitoring.metricWriter
```

1. Download a JSON key for the GSA
```bash
gcloud iam service-accounts keys create sa-key.json --iam-account=${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com
```

1. Create a K8s secret (Option 1)
```bash
kubectl -n gmp-public create secret generic gmp-sa-key --from-file=key.json=sa-key.json
```

1. Create a GCP Secret for use with External Secrets (Option 2)
```bash
gcloud secrets create gmp-writer --replication-policy="automatic"
gcloud secrets versions add gmp-writer --data-file="sa-key.json"
```

1. Either option, remove the JSON key after storing as `Secret` or Google Secret
```bash
rm -rf sa-key.json
```
